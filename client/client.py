# marketplace/marketplace.py

import os
from flask import Flask, Response, redirect, request, url_for,render_template
import grpc
from sample_pb2 import Category, GetBooksClientRequest
from sample_pb2_grpc import SampleServiceStub
import time

app = Flask(__name__)
server_host = os.getenv("SERVER_HOST", "localhost")
server_channel = grpc.insecure_channel(
    f"{server_host}:50051"
)
#SampleServiceStub is the name of the service defined in
#  the proto buffer + Stub
stub = SampleServiceStub(server_channel)
"""Stub :
The generated Stub class is used by the gRPC clients. 
It has a constructor that takes a grpc.Channel object 
and initializes the stub. For each method in the service, 
the initializer adds a corresponding attribute to the stub 
object with the same name. Depending on the RPC type 
(unary or streaming), the value of that attribute will be 
callable objects of type UnaryUnaryMultiCallable, 
UnaryStreamMultiCallable, StreamUnaryMultiCallable,
 or StreamStreamMultiCallabl"""

@app.route("/")
def render_homepage():
    get_books_request = GetBooksClientRequest(
        user_id=1, category=Category.MYSTERY, max_results=3
    )
    get_books_response = stub.GetBooks(get_books_request)

    return render_template(
        "homepage.html",
        books=get_books_response.books,
    )

@app.route("/server-stream")
def server_stream():
    get_books_request = GetBooksClientRequest(
        user_id=1, category=Category.MYSTERY
    )
    responses = stub.StreamBooks(get_books_request)
    def events():
        for response in responses:
            yield "data: %s \n\n" % (response)
            time.sleep(1)  # an artificial delay
    return Response(events(), content_type='text/event-stream')