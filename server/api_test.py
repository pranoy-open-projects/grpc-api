from server import SampleService
from sample_pb2 import Category, GetBooksClientRequest

def test_get_books():
    service = SampleService()
    request = GetBooksClientRequest(
        user_id=1, category=Category.MYSTERY, max_results=1
    )
    response = service.GetBooks(request, None)
    assert len(response.books) == 1

def test_stream_books():
    service = SampleService()
    request = GetBooksClientRequest(
        user_id=1, category=Category.MYSTERY, max_results=1
    )
    response = service.GetBooks(request, None)
    print(response)