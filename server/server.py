from concurrent import futures
import random
import grpc
from sample_pb2 import (
    Category,
    Book,
    GetBooksServerResponse,
    StreamBooksServerResponse,

)
import sample_pb2_grpc
from grpc_interceptor import ExceptionToStatusInterceptor
from grpc_interceptor.exceptions import NotFound



#Import book category and create BookRecommendation objects using that
books_by_category = {
    Category.MYSTERY: [
        Book(id=1, title="The Maltese Falcon"),
        Book(id=2, title="Murder on the Orient Express"),
        Book(id=3, title="The Hound of the Baskervilles"),
        Book(id=10, title="book10"),
        Book(id=11, title="book11"),
        Book(id=13, title="book12"),
        Book(id=14, title="book13"),
        Book(id=15, title="book14"),
        Book(id=16, title="book15"),
        Book(id=17, title="book16"),
        Book(id=18, title="book16"),
    ],
    Category.SCIENCE_FICTION: [
        Book(id=4, title="The Hitchhiker's Guide to the Galaxy"),
        Book(id=5, title="Ender's Game"),
        Book(id=6, title="The Dune Chronicles"),
    ],
    Category.SELF_HELP: [
        Book(id=7, title="The 7 Habits of Highly Effective People"),
        Book(id=8, title="How to Win Friends and Influence People"),
        Book(id=9, title="Man's Search for Meaning"),
    ],
}
#SampleServiceServicer is the name of the service defined in
#  the proto buffer + Servicer
class SampleService(
    sample_pb2_grpc.SampleServiceServicer
):  
    #Methods being invoked by the proto buffer

    #Unary  : Get list of books
    def GetBooks(self, request, context):
        if request.category not in books_by_category:
            context.abort(grpc.StatusCode.NOT_FOUND, "Category not found")
        books_for_category = books_by_category[request.category]
        num_results = min(request.max_results, len(books_for_category))
        books_to_recommend = random.sample(
            books_for_category, num_results
        )

        return GetBooksServerResponse(books=books_to_recommend)
    # Server Side streaming
    def StreamBooks(self, request, context):
        if request.category not in books_by_category:
            context.abort(grpc.StatusCode.NOT_FOUND, "Category not found")
        books_for_category = books_by_category[request.category]
        for book in books_for_category:
            yield StreamBooksServerResponse(book=book)
        
    def ClientStream(self, request_iterator, context):
        pass
    
    def BiStream(elf, request_iterator, context):
        pass


#Starts the server
def serve():
    interceptors = [ExceptionToStatusInterceptor()]

    server = grpc.server(
        futures.ThreadPoolExecutor(max_workers=10),
        interceptors=interceptors
    )
    #SampleServiceServicer is the name of the service defined in
    #  the proto buffer + Servicer
    # Here we are adding this service to the server
    sample_pb2_grpc.add_SampleServiceServicer_to_server(
        SampleService(), server
    )
    server.add_insecure_port("[::]:50051")
    server.start()
    server.wait_for_termination()

"""Servicer
For each service, a Servicer class is generated, 
which serves as the superclass of a service implementation. 
For each method in the service, a corresponding function in 
the Servicer class is generated. Override this function with 
the service implementation. Comments associated with code 
elements in the .proto file appear as docstrings in the 
generated python code"""

if __name__ == "__main__":
    serve()